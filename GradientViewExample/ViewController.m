//
//  ViewController.m
//  GradientViewExample
//
//  Created by fisherman on 2022/1/13.
//

#import "ViewController.h"
#import "UIViewExtension.h"
#import <Masonry.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIGradientView *gradientView = [UIGradientView new];
//    gradientView.colors = @[(__bridge id)(UIColor.redColor.CGColor), (__bridge id)(UIColor.greenColor.CGColor)];
    gradientView.colors = @[UIColor.redColor, UIColor.greenColor];
    gradientView.locations = @[@0.25, @0.75];
    [self.view addSubview:gradientView];
    
    [gradientView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.mas_equalTo(200);
        make.height.mas_equalTo(100);
    }];
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 100, 100)];
    UIShapeView *shapeView = [UIShapeView new];
    shapeView.path = bezierPath.CGPath;
    shapeView.strokeColor = [UIColor redColor];
    shapeView.fillColor = UIColor.clearColor;
    shapeView.lineWidth = 15;
    shapeView.lineCap = kCALineCapButt;
    shapeView.lineJoin = kCALineJoinBevel;
    [self.view addSubview:shapeView];
    [shapeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(gradientView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(100);
    }];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = UIColor.orangeColor;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    button.timeInterval = 2;
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(shapeView.mas_bottom).offset(20);
        make.left.right.equalTo(self.view).inset(20);
        make.height.mas_equalTo(30);
    }];
}
- (void)buttonAction:(UIButton *)button {
    NSLog(@"---button clicked");
}


@end
