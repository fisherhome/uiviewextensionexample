//
//  UIShapeView.m
//  GradientViewExample
//
//  Created by fisherman on 2022/1/12.
//

#import "UIShapeView.h"

@implementation UIShapeView

#pragma mark getter
+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (CGPathRef)path {
    return ((CAShapeLayer *)self.layer).path;
}

- (UIColor *)fillColor {
    return [UIColor colorWithCGColor:((CAShapeLayer *)self.layer).fillColor];
}

- (CAShapeLayerFillRule)fillRule {
    return ((CAShapeLayer *)self.layer).fillRule;
}

- (UIColor *)strokeColor {
    return [UIColor colorWithCGColor:((CAShapeLayer *)self.layer).strokeColor];
}

- (CGFloat)strokeStart {
    return ((CAShapeLayer *)self.layer).strokeStart;
}

- (CGFloat)strokeEnd {
    return ((CAShapeLayer *)self.layer).strokeEnd;
}

- (CGFloat)lineWidth {
    return ((CAShapeLayer *)self.layer).lineWidth;
}

- (CGFloat)miterLimit {
    return ((CAShapeLayer *)self.layer).miterLimit;
}

- (CAShapeLayerLineCap)lineCap {
    return ((CAShapeLayer *)self.layer).lineCap;
}

- (CAShapeLayerLineJoin)lineJoin {
    return ((CAShapeLayer *)self.layer).lineJoin;
}

- (CGFloat)lineDashPhase {
    return ((CAShapeLayer *)self.layer).lineDashPhase;
}

- (NSArray<NSNumber *> *)lineDashPattern {
    return ((CAShapeLayer *)self.layer).lineDashPattern;
}

#pragma mark setter
- (void)setPath:(CGPathRef)path {
    ((CAShapeLayer *)self.layer).path = path;
}

- (void)setFillColor:(UIColor *)fillColor {
    ((CAShapeLayer *)self.layer).fillColor = fillColor.CGColor;
}

- (void)setFillRule:(CAShapeLayerFillRule)fillRule {
    ((CAShapeLayer *)self.layer).fillRule = fillRule;
}

- (void)setStrokeColor:(UIColor *)strokeColor {
    ((CAShapeLayer *)self.layer).strokeColor = strokeColor.CGColor;
}

- (void)setStrokeStart:(CGFloat)strokeStart {
    ((CAShapeLayer *)self.layer).strokeStart = strokeStart;
}

- (void)setStrokeEnd:(CGFloat)strokeEnd {
    ((CAShapeLayer *)self.layer).strokeEnd = strokeEnd;
}

- (void)setLineWidth:(CGFloat)lineWidth {
    ((CAShapeLayer *)self.layer).lineWidth = lineWidth;
}

- (void)setMiterLimit:(CGFloat)miterLimit {
    ((CAShapeLayer *)self.layer).miterLimit = miterLimit;
}

- (void)setLineCap:(CAShapeLayerLineCap)lineCap {
    ((CAShapeLayer *)self.layer).lineCap = lineCap;
}

- (void)setLineJoin:(CAShapeLayerLineJoin)lineJoin {
    ((CAShapeLayer *)self.layer).lineJoin = lineJoin;
}

- (void)setLineDashPhase:(CGFloat)lineDashPhase {
    ((CAShapeLayer *)self.layer).lineDashPhase = lineDashPhase;
}

- (void)setLineDashPattern:(NSArray<NSNumber *> *)lineDashPattern {
    ((CAShapeLayer *)self.layer).lineDashPattern = lineDashPattern;
}

@end
