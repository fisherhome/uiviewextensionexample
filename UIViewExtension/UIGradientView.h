//
//  UIGradientView.h
//  UIGradientView
//
//  Created by fisherman on 2022/1/12.
//

#import <UIKit/UIKit.h>

@interface UIGradientView : UIView

@property (nonatomic, nullable, copy) NSArray *colors;
@property (nonatomic, nullable, copy) NSArray<NSNumber *> *locations;
@property (nonatomic, assign) CGPoint startPoint;
@property (nonatomic, assign) CGPoint endPoint;
@property (nonatomic, nullable, copy) CAGradientLayerType type;

@end
