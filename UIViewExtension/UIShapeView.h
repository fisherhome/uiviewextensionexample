//
//  UIShapeView.h
//  GradientViewExample
//
//  Created by fisherman on 2022/1/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIShapeView : UIView

@property(nullable) CGPathRef path;
@property(nullable) UIColor *fillColor;
@property(copy) CAShapeLayerFillRule fillRule;
@property(nullable) UIColor *strokeColor;
@property CGFloat strokeStart;
@property CGFloat strokeEnd;
@property CGFloat lineWidth;
@property CGFloat miterLimit;
@property(copy) CAShapeLayerLineCap lineCap;
@property(copy) CAShapeLayerLineJoin lineJoin;
@property CGFloat lineDashPhase;
@property(nullable, copy) NSArray<NSNumber *> *lineDashPattern;

@end

NS_ASSUME_NONNULL_END
