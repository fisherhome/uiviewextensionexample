#import "UIButton+Touch.h"
#import <objc/runtime.h>

@interface UIButton()
@property (nonatomic, assign) NSTimeInterval touchTime;
@end

@implementation UIButton (Touch)
@dynamic timeInterval;

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL selA = @selector(sendAction:to:forEvent:);
        SEL selB = @selector(swizzlingSendAction:to:forEvent:);
        Method methodA =   class_getInstanceMethod(self,selA);
        Method methodB = class_getInstanceMethod(self, selB);
        //将 methodB的实现 添加到系统方法中 也就是说 将 methodA方法指针添加成 方法methodB的  返回值表示是否添加成功
        BOOL isAdd = class_addMethod(self, selA, method_getImplementation(methodB), method_getTypeEncoding(methodB));
        //添加成功了 说明 本类中不存在methodB 所以此时必须将方法b的实现指针换成方法A的，否则 b方法将没有实现。
        if (isAdd) {
            class_replaceMethod(self, selB, method_getImplementation(methodA), method_getTypeEncoding(methodA));
        } else {
            //添加失败了 说明本类中 有methodB的实现，此时只需要将 methodA和methodB的IMP互换一下即可。
            method_exchangeImplementations(methodA, methodB);
        }
    });
}

//runtime 动态绑定 属性
- (NSTimeInterval)timeInterval {
    return [objc_getAssociatedObject(self, _cmd) doubleValue];
}
- (void)setTimeInterval:(NSTimeInterval)timeInterval {
    objc_setAssociatedObject(self, @selector(timeInterval), @(timeInterval), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//runtime 动态绑定 属性
- (NSTimeInterval)touchTime {
    return [objc_getAssociatedObject(self, _cmd) doubleValue];
}
- (void)setTouchTime:(NSTimeInterval)touchTime {
    objc_setAssociatedObject(self, @selector(touchTime), @(touchTime), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//当我们按钮点击事件 sendAction:to:forEvent: 时, 将会执行 swizzlingSendAction:to:forEvent:
- (void)swizzlingSendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    if ([NSStringFromClass(self.class) isEqualToString:@"UIButton"]) {
        
        NSTimeInterval currentTouchTime = [NSDate.date timeIntervalSince1970]*1000;
        NSTimeInterval interval = currentTouchTime - self.touchTime;
        if (self.touchTime == 0 || interval > self.timeInterval*1000) {
            self.touchTime = currentTouchTime;
            
            //此处 methodA和methodB方法IMP互换了，实际上执行 sendAction:to:forEvent: 所以不会死循环
            [self swizzlingSendAction:action to:target forEvent:event];
        }
    }
}

@end
