//
//  UIGradientView.m
//  UIGradientView
//
//  Created by fisherman on 2022/1/12.
//

#import "UIGradientView.h"

@implementation UIGradientView

#pragma mark getter
+ (Class)layerClass {
    return [CAGradientLayer class];
}
- (NSArray *)colors {
    return ((CAGradientLayer *)self.layer).colors;
}
- (NSArray<NSNumber *> *)locations {
    return ((CAGradientLayer *)self.layer).locations;
}
- (CGPoint)startPoint {
    return ((CAGradientLayer *)self.layer).startPoint;
}
- (CGPoint)endPoint {
    return ((CAGradientLayer *)self.layer).endPoint;
}
- (CAGradientLayerType)type {
    return ((CAGradientLayer *)self.layer).type;
}
#pragma mark setter
- (void)setColors:(NSArray *)colors {
    NSMutableArray *colorsArray = colors.mutableCopy;
    [colors enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:UIColor.class]) {
            UIColor *color = (UIColor *)obj;
            colorsArray[idx] = (__bridge id)color.CGColor;
        }
    }];
    ((CAGradientLayer *)self.layer).colors = colorsArray;
}
- (void)setLocations:(NSArray<NSNumber *> *)locations {
    ((CAGradientLayer *)self.layer).locations = locations;
}
- (void)setStartPoint:(CGPoint)startPoint {
    ((CAGradientLayer *)self.layer).startPoint = startPoint;
}
- (void)setEndPoint:(CGPoint)endPoint {
    ((CAGradientLayer *)self.layer).endPoint = endPoint;
}
- (void)setType:(CAGradientLayerType)type {
    ((CAGradientLayer *)self.layer).type = type;
}

@end
